from collections import defaultdict

f = open('input', 'r')
init_l = []
give_l = []
for line in f:
    line = line.rstrip()
    if line[0] == 'v':
        init_l.append(line)
    else:
        give_l.append(line)

bots = defaultdict(list)
for init in init_l:
    init = init.split(' ')
    bots[int(init[5])].append(int(init[1]))

insts = {}
for give in give_l:
    give = give.split(' ')
    insts[int(give[1])] = [
        {'type': give[5], 'val': int(give[6])},
        {'type': give[10], 'val': int(give[11])},
    ]

key_bot = None
outputs = {}
while True:
    inst = None
    matching_chip_l = None
    for bot, chip_l in bots.items():
        if len(chip_l) == 2:
            chip_l.sort()
            if chip_l == [17, 61]:
                key_bot = bot

            inst = insts[bot]
            matching_chip_l = chip_l
            break

    if inst is None:
        break

    for out in inst:
        if out['type'] == 'bot':
            bots[out['val']].append(matching_chip_l.pop(0))
        else:
            outputs[out['val']] = matching_chip_l.pop(0)

print("Part One: {}".format(key_bot))
print("Part Two: {}".format(outputs[0] * outputs[1] * outputs[2]))
