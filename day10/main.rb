init_l = []
give_l = []
ARGF.each do |line|
    line = line.rstrip
    if line[0] == 'v'
        init_l.push(line)
    else
        give_l.push(line)
    end
end

bots = Hash.new { |h, k| h[k] = [] }
init_l.each do |init|
    init = init.split(' ')
    bots[Integer(init[5])].push(Integer(init[1]))
end

insts = Hash.new
give_l.each do |give|
    give = give.split(' ')
    insts[Integer(give[1])] = [
        {:type => give[5], :val => Integer(give[6])},
        {:type => give[10], :val => Integer(give[11])},
    ]
end

key_bot = nil
outputs = Hash.new
while true do
    inst = nil
    matching_chip_l = nil
    bots.each do |bot, chip_l|
        if chip_l.length == 2
            chip_l.sort!
            if chip_l == [17, 61]
                key_bot = bot
            end

            inst = insts[bot]
            matching_chip_l = chip_l
            break
        end
    end

    break if inst == nil

    inst.each do |out|
        if out[:type] == 'bot'
            bots[out[:val]].push(matching_chip_l.shift)
        else
            outputs[out[:val]] = matching_chip_l.shift
        end
    end
end

puts "Part One: #{ key_bot }"
puts "Part Two: #{ outputs[0] * outputs[1] * outputs[2] }"
