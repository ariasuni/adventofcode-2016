cmds = ARGF.read.split("\n")

digipad = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]]
coord = [1, 1]

digits = []
cmds.each do |cmd|
    cmd.chars.each do |letter|
        coord_axe = (letter == 'U' || letter == 'D')? 0: 1
        increment = (letter == 'U' || letter == 'L')? -1: 1
        coord[coord_axe] += increment
        if coord[coord_axe] < 0 || coord[coord_axe] > 2
            coord[coord_axe] -= increment
        end
    end
    digits.push(digipad[ coord[0] ][ coord[1] ])
end
puts "Part One: #{ digits.join }"


digipad = [
    %w(0 0 1 0 0),
    %w(0 2 3 4 0),
    %w(5 6 7 8 9),
    %w(0 A B C 0),
    %w(0 0 D 0 0)]
coord = [2, 0]

digits = []
cmds.each do |cmd|
    cmd.chars.each do |letter|
        coord_axe = (letter == 'U' || letter == 'D')? 0: 1
        coord_axe2 = (coord_axe + 1) % 2
        increment = (letter == 'U' || letter == 'L')? -1: 1
        coord[coord_axe] += increment
        if coord[coord_axe] < 0 || coord[coord_axe] > 4 ||
                ((coord[coord_axe2] == 1 || coord[coord_axe2] == 3) &&
                    (coord[coord_axe] == 0 || coord[coord_axe] == 4)) ||
                ((coord[coord_axe2] == 0 || coord[coord_axe2] == 4) &&
                    (coord[coord_axe] == 1 || coord[coord_axe] == 3))
            coord[coord_axe] -= increment
        end
    end
    digits.push(digipad[ coord[0] ][ coord[1] ])
end
puts "Part Two: #{ digits.join }"
