def more_freq_letters(letters)
    freqs = Hash.new(0)
    for letter in letters.split('') do
        freqs[letter] += 1
    end
    freqs.to_a.sort! { |a, b| b[1] == a[1]? a[0] <=> b[0]: b[1] <=> a[1] }.
            map { |x| x[0] }.slice!(0 .. 4)
end

def shift(str, offset)
    offset %= 26
    az = ('a'..'z').to_a
    str.chars.map { |c|
        c == '-'? ' ': az[az.find_index(c) + offset - az.size]
    }.join
end

rooms = []
ARGF.read.split("\n").each { |line|
    checksum = line.slice(line.length - 6, 5)
    num = line.slice(line.length - 10, 3).to_i
    letters = line.slice!(0 .. line.length - 12)
    rooms.push([letters, num, checksum])
}

sum = 0
valid_rooms = []
rooms.each { |room|
    if room[2] == more_freq_letters(room[0].delete('-')).join
        sum += room[1]
        valid_rooms.push(room)
    end
}

northpole_room = nil
valid_rooms.each { |room|
    if shift(room[0], room[1]).include? "northpole"
        northpole_room = room[1].to_s
    end
}

puts "Part One: #{ sum.to_s }"
puts "Part Two: #{ northpole_room.to_s }"
