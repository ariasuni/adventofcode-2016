addrs = ARGF.read.split("\n")

def has_abba(s)
    (3..s.length - 1).each do |i|
        if s[i - 3] != s[i - 2] && s[i - 3] == s[i] && s[i - 2] == s[i - 1]
            return true
        end
    end
    false
end

def list_aba(s)
    l = []
    (2..s.length - 1).each do |i|
        if s[i - 2] == s[i]
            l.push(s[i - 2..i])
        end
    end
    l
end

tls_count = 0
tls_count2 = 0
addrs.each do |addr|
    core_l = addr.gsub(/[^\[]*\]/, '').split('[')
    hypernet_l = addr.scan(/\[[^\]]*\]/).map { |s| s[1..s.length - 2] }

    if core_l.any? { |s| has_abba(s) } && !hypernet_l.any? { |s| has_abba(s) }
        tls_count += 1
    end
    if (core_l.flat_map { |s| list_aba(s) }.map { |s| s[1]+s[0]+s[1] } &
            hypernet_l.flat_map { |s| list_aba(s) }).length != 0
        tls_count2 += 1
    end
end

puts "Part One: #{ tls_count }"
puts "Part Two: #{ tls_count2 }"
