def dec_len(enc, vers)
    len = 0
    i = 0
    while true do
        par_i = enc.index('(', i)
        if par_i != nil
            len += par_i - i  # non compressed

            j = par_i + 1
            until enc[j] == ')'
                j += 1
            end
            marker = enc.slice(par_i + 1...j)
            nb_ch, repeat = marker.split('x').map { |s| s.to_i }

            inner_len = (vers == 2)? dec_len(enc.slice(j + 1, nb_ch), 2): nb_ch
            len += repeat * inner_len

            i = j + nb_ch + 1
        else
            return len + enc.length - i
        end
    end
end

enc = ARGF.read.strip

puts "Part One: #{ dec_len(enc, 1) }"
puts "Part Two: #{ dec_len(enc, 2) }"
