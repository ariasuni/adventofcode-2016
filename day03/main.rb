def count_possible(triplets)
    possible_cnt = 0
    for triplet in triplets do
        if triplet[0] + triplet[1] > triplet[2] &&
                triplet[0] + triplet[2] > triplet[1] &&
                triplet[1] + triplet[2] > triplet[0]
            possible_cnt += 1
        end
    end
    return possible_cnt
end

triplets = ARGF.read.split("\n").
        map { |str| str.split(' ').map { |i| i.to_i } }
triplets2 = []

i = 0
until i == triplets.length do
    (0..2).each do |offset|
        triplet = []
        (0..2).each do |n|
            triplet.push(triplets[i + n][offset])
        end
        triplets2.push(triplet)
    end
    i += 3
end

puts "Part One: #{ count_possible(triplets).to_s }"
puts "Part Two: #{ count_possible(triplets2).to_s }"
