cmds = ARGF.read.split("\n")

height = 6
display = Array.new()
height.times { display.push(Array.new(50, false)) }

i = 0
cmds.each do |cmd|
    i += 1
    words = cmd.split(' ')
    if words[0] == "rect"
        wide, tall = words[1].split('x').map { |s| s.to_i }
        # print "#{ wide } #{ tall }\n"
        (0...tall).each do |row|
            (0...wide).each do |col|
                display[row][col] = true
            end
        end
    elsif words[0] == "rotate"
        index = words[2].split('=')[1].to_i
        count = words[4].to_i
        if words[1] == "row"
            display[index].rotate!(-count)
        else
            tmp = []
            (height-count...height).each do |row_i|
                tmp.push(display[row_i][index])
            end
            (height-1-count).downto(0).each do |row_i|
                display[row_i + count][index] = display[row_i][index]
            end
            (0...count).each do |row_i|
                display[row_i][index] = tmp[row_i]
            end
        end
    end
end

puts "Part One: #{ display.flat_map { |row| row }.count { |e| e } }"
puts "Part Two:"
display.each do |row|
    r = row.map { |val| if val then '#' else ' ' end }
    puts r.join(' ')
end
