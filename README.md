# Advent of Code 2016 in Ruby

Each folder contains a solution as a single `main.rb` source file which computes both parts of the
day’s challenge for the `input` file.

Simply [install Ruby](https://www.ruby-lang.org/en/downloads/), then run `ruby main.rb input` or
`ruby main.rb < input` (you can replace `input` with the file containing your input) in the folder
of the day you’re interested in. The executable then prints the two solutions in the terminal.
