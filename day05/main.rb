require 'digest'

door_id = ARGF.read.strip

md5 = Digest::MD5.new
salt = 0
pass = []
pass2 = Array.new(8, '')
pass2_size = 0
begin
    digest = md5.hexdigest(door_id + salt.to_s)
    salt += 1
    if digest.slice(0, 5) == "00000"
        if pass.length < 8
            pass.push(digest[5])
        end
        position = Integer(digest[5]) rescue -1
        if position >= 0 && position <= 7 && pass2[position] == ''
            pass2[position] = digest[6]
            pass2_size += 1
        end
    end
end until pass2_size == 8

puts "Part One: #{ pass.join }"
puts "Part Two: #{ pass2.join }"
