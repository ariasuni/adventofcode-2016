cmds = ARGF.read.split(", ")

angle = 1
coord = [0, 0] # coord[0] = east and coord[1] = north (angle in π/2)
coords_hist = []
first_twice = []

cmds.each do |cmd|
    angle = (angle + (cmd[0] == 'L'? 1: -1)) % 4
    blocks = cmd[1..-1].to_i
    coord_axe = (angle % 2 == 0)? 0: 1
    # if east (0) and angle is 0 or north (1) and angle is 1 then +, else -
    increment = (angle == coord_axe)? 1: -1
    until blocks == 0 do
        coord[coord_axe] += increment
        if first_twice == []
            if coords_hist.include? coord
                first_twice = coord.dup
            else
                coords_hist.push(coord.dup)
            end
        end
        blocks -= increment.abs
    end
end

puts "Part One: #{ coord.map{ |n| n.abs }.reduce(:+).to_s }"
puts "Part Two: #{ first_twice.map{ |n| n.abs }.reduce(:+).to_s }"
