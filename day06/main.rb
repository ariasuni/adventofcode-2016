msgs = ARGF.read.split("\n")

final_msg = []
final_msg2 = []
msgs[0].length.times do |time|
    letters = msgs.map { |msg| msg[time] }
    freq = letters.inject(Hash.new(0)) { |h, v| h[v] +=1; h }
    final_msg.push(letters.max_by { |v| freq[v] })
    final_msg2.push(letters.min_by { |v| freq[v] })
end

puts "Part One: #{ final_msg.join }"
puts "Part Two: #{ final_msg2.join }"
